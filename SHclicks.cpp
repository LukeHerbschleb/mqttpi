#include "Pixel.h"
#include "CommandProcessor.h"
#include "SHclicks.h"
#include "GATTclicks.h"
#include "GATTwrapper.h"

#include <cstring>
#include <string>

SHclicks::SHclicks():
   SenseHAT()
{
   leds.clear();
}

SHclicks::~SHclicks()
{
   leds.clear(Pixel(40, 0, 0));
}

void SHclicks::initDisplay()
{
   leds.clear(Pixel(50, 50, 80));
}

void SHclicks::setLed_MQTTconnected(const Pixel &pixel)
{
   leds.setPixel(0, 0, pixel);
}

void SHclicks::setLed_BLElogo(const Pixel &pixel)
{
   leds.setPixel(2, 1, pixel);
   leds.setPixel(3, 1, pixel);
   leds.setPixel(1, 2, pixel);
   leds.setPixel(1, 3, pixel);
   leds.setPixel(2, 4, pixel);
   leds.setPixel(3, 4, pixel);

   leds.setPixel(5, 3, pixel);
   leds.setPixel(6, 3, pixel);
   leds.setPixel(4, 4, pixel);
   leds.setPixel(4, 5, pixel);
   leds.setPixel(5, 6, pixel);
   leds.setPixel(6, 6, pixel);
}


void SHclicks::setLed_BLEconnected(const Pixel &pixel)
{
   leds.setPixel(0, 1, pixel);
}

void SHclicks:: error(){
   leds.clear(Pixel(255, 0, 0));
}

void SHclicks:: temperature(int temp, int humi, int pres){
   leds.clear(Pixel(50, 50, 80));

   if (temp <= 100 && temp > 0){
   temp = temp * 6 / 100; 
   for (int x = 1; temp >=x; x++){
   leds.setPixel(x, 2, Pixel(200, 0, 0));
   leds.setPixel(x, 3, Pixel(200, 0, 0));
   }
   }

   if (humi <= 100 && humi > 0){
      humi = humi * 6 / 100; 
   for (int x = 1; humi >=x; x++){
   leds.setPixel(x, 4, Pixel(0, 0, 255));
   leds.setPixel(x, 5, Pixel(0, 0, 255));
   }
   }

   if (pres <= 100 && pres > 0){
   pres = pres * 6 / 100; 
   for (int x = 1; pres >=x; x++){
   leds.setPixel(x, 6, Pixel(200, 100, 0));
   leds.setPixel(x, 7, Pixel(200, 100, 0));
   }
   }
}

void SHclicks:: pause(int temp, int humi, int pres){
   leds.clear(Pixel(50, 50, 80));
   temp = temp * 6 / 100; 
   for (int x = 1; temp >=x; x++){
   leds.setPixel(x, 2, Pixel(255, 255, 0));
   leds.setPixel(x, 3, Pixel(255, 255, 0));
   }
   
   humi = humi * 6 / 100; 
   for (int x = 1; humi >=x; x++){
   leds.setPixel(x, 4, Pixel(255, 255, 0));
   leds.setPixel(x, 5, Pixel(255, 255, 0));
   }
   
   pres = pres * 6 / 100; 
   for (int x = 1; pres >=x; x++){
   leds.setPixel(x, 6, Pixel(255, 255, 0));
   leds.setPixel(x, 7, Pixel(255, 255, 0));
   }
}

void SHclicks:: stop(){
   leds.clear(Pixel(50, 50, 80));
}

void SHclicks:: showLine(int line){
              switch (line)
        {
           case 0:
            leds.setPixel(0, 2, Pixel(200, 10, 0));
            leds.setPixel(0, 4, Pixel(50, 50, 80));
            leds.setPixel(0, 6, Pixel(50, 50, 80));
           break;
           case 1:
            leds.setPixel(0, 4, Pixel(200, 10, 0));
            leds.setPixel(0, 2, Pixel(50, 50, 80));
            leds.setPixel(0, 6, Pixel(50, 50, 80));
           break;
           case 2:
            leds.setPixel(0, 6, Pixel(200, 10, 0));
            leds.setPixel(0, 2, Pixel(50, 50, 80));
            leds.setPixel(0, 4, Pixel(50, 50, 80));
           break;
           default:

           break;
        }
}

