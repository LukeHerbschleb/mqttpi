// ----------------------------------------------------------------------------
// Wrapper for gatttool
//
// By Hugo Arend
// September 2017
// ----------------------------------------------------------------------------
#include "gatt_wrapper.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>

// ----------------------------------------------------------------------------
// Local variables
// ----------------------------------------------------------------------------
static int fdX[2];
static int fdY[2];

// ----------------------------------------------------------------------------
// Function: gatt_start
//
// Description:
//   This function starts the gatt wrapper.
//   The gatttool is started in a child process. stdin and stdout of this child
//   process are connected to the parent process through pipes.
//   The parent process must start a separate thread for reading lines from the
//   child (gatttool). This ensures that the read operation is non-blocking.
// ----------------------------------------------------------------------------
void gatt_start(void)
{
   pid_t cpid;

   if(pipe(fdX) == -1)
   {
      fprintf(stderr, "Error: could not create pipe!\n");
      exit(EXIT_FAILURE);
   }

   if(pipe(fdY) == -1)
   {
      fprintf(stderr, "Error: could not create pipe!\n");
      exit(EXIT_FAILURE);
   }

   cpid = fork();

   if(cpid == -1)
   {
      fprintf(stderr, "Error: could not start a child process!\n");
      exit(EXIT_FAILURE);
   }

   if(cpid == 0)
   {
      // CHILD PROCESS ---------------------------------------------------------
      // Close unused file descriptors
      close(fdX[1]);
      close(fdY[0]);

      // Connect child's stdin to pipe X output
      //
      //         fdX[0]    stdin
      //           |         |_______
      //  =========]-------->[_______|
      //    pipe X            CHILD
      //
      close(0);
      dup(fdX[0]);

      // Connect child's stdout to pipe Y input
      //
      //         fdY[1]    stdout
      //           |         |_______
      //  =========]<--------[_______|
      //    pipe Y             CHILD
      //
      close(1);
      dup(fdY[1]);

      // Start gatttool in interactive mode
      int ret = execl("/usr/bin/gatttool", "gatttool", "-I", NULL);

      if(ret == -1)
      {
         fprintf(stderr, "Error: could not start gatttool!\n");
         exit(EXIT_FAILURE); // ??? Next lines are not executed
      }

      close(fdX[0]);
      close(fdY[1]);

      // Child stops
      exit(0);
   }

   // PARENT PROCESS -----------------------------------------------------------
   // Close unused file descriptors
   close(fdX[0]);
   close(fdY[1]);

   // From this point on, write to child through pipe X
   //
    //       write()     fdX[1]
    //   ________|         |
    //  |________]-------->[=========
    //    PARENT             pipe X

    // From this point on, read from child through pipe Y
    //
    //        read()     fdY[0]
    //   ________|         |
    //  |________]<--------[=========
    //    PARENT             pipe Y
}

// ----------------------------------------------------------------------------
// Function: gatt_exit
//
// Description:
//   This function stops the gatt wrapper.
//   The gatttool is stopped, the thread is cancelled and the function waits
//   untill the thread has been stopped.
// ----------------------------------------------------------------------------
int gatt_exit(void)
{
   DBG("gatt_exit", "\n");
   gatt_write("exit\n");

   return 0;
}

// ----------------------------------------------------------------------------
// Function: gatt_write
//
// Description:
//   This function writes the string s to the gatttool.
//   gatttool must have been started with the gatt_start() function prior to
//   using this function.
// ----------------------------------------------------------------------------
void gatt_write(const char *s)
{
   DBG("gatt_write", s);
   write(fdX[1], s, strlen(s));
}

// ----------------------------------------------------------------------------
// Function: gatt_read
//
// Description:
//   This function reads a string from gatttool and places it in r.
//   The function returns as soon as a CR or LF is detected.
// ----------------------------------------------------------------------------
void gatt_read(char *r)
{
   unsigned char c;
   unsigned int i = 0;

   r[0] = '\0';
   do
   {
      // Read one character from gatttool
      read(fdY[0], &c, 1);
      // Replace CR for LF
      if(c == '\r')
         c = '\n';

      // Save the character in the response string
      r[i++] = c;

      //printf("%02X ", c); // for debugging
    } while(c != '\n');

   //printf("\n"); // for debugging

   // Terminate the response string
   r[i] = '\0';
}
