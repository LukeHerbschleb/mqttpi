#ifndef GATTCLICKS_H
#define GATTCLICKS_H

#include "GATTwrapper.h"
#include <string>

class CommandProcessor;
class SHclicks;

/// \todo Add documentation text.
class GATTclicks: public GATTwrapper
{
public:

   GATTclicks(SHclicks &shClicks,
              CommandProcessor &cmdpr,
              int nTriesConnecting,
              const std::string &BLEaddress);
   virtual ~GATTclicks() = default;

   SHclicks &shClicks;

   void enableClicksNotifications();
   void disableClicksNotifications();
   void setAlertLevel(int level);
   void temperature(int temp);
   void humidity(int humi);
   void pressure(int pres);
   void error();
   void start();
   void pause();
   void stop();

   unsigned int clicks = 0;

protected:
   volatile unsigned short ias_value_handle;
   volatile unsigned short clicks_value_handle;
   volatile unsigned short manufacturer_name_string_value_handle;
   volatile unsigned short serial_number_string_value_handle;

   virtual void connectCB() override;
   virtual void charsCB(const char *uuid_128,
                        unsigned short char_handle,
                        unsigned char properties, unsigned short value_handle)
                        override;
   virtual void charDescCB(unsigned short attribute_handle,
                           const char *uuid_128) override;
   virtual void charReadHndCB(unsigned char *data, int len) override;
   virtual void charWriteReqCB(int succesful) override;
   virtual void charReadUuidCB(unsigned short value_handle,
                               unsigned char *data, int len) override;
   virtual void notifyCB(unsigned short value_handle,
                         unsigned char *data, int len) override;
};

#endif
