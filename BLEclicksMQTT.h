#ifndef BLEclicksMQTTMQTT_H
#define BLEclicksMQTTMQTT_H

#include "CommandProcessor.h"
#include "GATTclicks.h"
#include "SHclicks.h"
#include "SenseHAT.h"
#include <string>
#include <vector>

using parameters_t = std::vector<std::string>;

/// @todo Add documentation text.
class BLEclicksMQTT: public CommandProcessor
{
public:
   BLEclicksMQTT(const std::string& bleMAC,
                 const std::string& appname,
                 const std::string& clientname,
                 const std::string& host,
                 int port);
   virtual ~BLEclicksMQTT() = default;
   BLEclicksMQTT(const BLEclicksMQTT &other) = delete;
   BLEclicksMQTT& operator=(const BLEclicksMQTT &other) = delete;

   SHclicks shClicks;
   GATTclicks gattClicks;

private:
   /// Immediate Alert Service (IAS)
   void setAlertLevel(const parameters_t& commandParameters);
   /// Notifications
   void setClickNotifications(const parameters_t& commandParameters);

   void temperature(const parameters_t& commandParameters);
   void humidity(const parameters_t& commandParameters);
   void pressure(const parameters_t& commandParameters);
   void state(const parameters_t& commandParameters);
   void error(const parameters_t& commandParameters);

   void jsup();
   void jsdown();
   void jsright();
   void jsleft();
   void jspressed();

   int temp = 0;
   int humi = 0;
   int pres = 0;

   int line = 0;

   bool start_state = false;
};

#endif

