#include "BLEconfig.h"
#include "BLEclicksMQTT.h"
#include "Pixel.h"
#include "SenseHAT.h"

#include <stdexcept>
#include <iostream>
#include <string>

BLEclicksMQTT::BLEclicksMQTT(const std::string& bleMAC,
                     const std::string& appname,
                     const std::string& clientname,
                     const std::string& host,
                     int port):
   CommandProcessor{appname, clientname, host, port},
   shClicks{},
   gattClicks{shClicks, *this, BLE_MAX_TRIES_CONNECTING, bleMAC}
{
   shClicks.initDisplay();
   shClicks.setLed_BLElogo(Pixel::GREEN);
   shClicks.setLed_MQTTconnected(Pixel::RED);
   shClicks.setLed_BLEconnected(Pixel::RED);
   registerCommand("setAlertLevel",
                   bind(&BLEclicksMQTT::setAlertLevel, this,
                        std::placeholders::_1));
   registerCommand("setClickNotifications",
                   bind(&BLEclicksMQTT::setClickNotifications, this,
                        std::placeholders::_1));
   registerCommand("temperature",
                   bind(&BLEclicksMQTT::temperature, this,
                        std::placeholders::_1));
   registerCommand("humidity",
                   bind(&BLEclicksMQTT::humidity, this,
                        std::placeholders::_1));
   registerCommand("pressure",
                   bind(&BLEclicksMQTT::pressure, this,
                        std::placeholders::_1));
   registerCommand("state",
                   bind(&BLEclicksMQTT::state, this,
                        std::placeholders::_1));                    
   registerCommand("error",
                   bind(&BLEclicksMQTT::error, this,
                        std::placeholders::_1));

   shClicks.stick.directionUP = std::bind(&BLEclicksMQTT::jsup, this);
   shClicks.stick.directionDOWN = std::bind(&BLEclicksMQTT::jsdown, this);
   shClicks.stick.directionLEFT = std::bind(&BLEclicksMQTT::jsleft, this);
   shClicks.stick.directionRIGHT = std::bind(&BLEclicksMQTT::jsright, this);
   shClicks.stick.directionPRESSED = std::bind(&BLEclicksMQTT::jspressed, this);

}

void BLEclicksMQTT::setAlertLevel(const parameters_t& commandParameters)
{
  if (commandParameters.size() != 1)
  {
     publishError("setAlertLevel", "number of parameters != 1");
  }
  else
  {
     try
     {
        switch (stoi(commandParameters[0]))
        {
           case 0:
           case 1:
           case 2:
              gattClicks.setAlertLevel(stoi(commandParameters[0]));
              break;
           default:
              publishError("setAlertLevel", "parameter wrong value");
        }
     }
     catch(std::invalid_argument &e)
     {
         publishError("setAlertLevel", "invalid format argument");
     }
  }
}

void BLEclicksMQTT::setClickNotifications(const parameters_t& commandParameters)
{
   if (commandParameters.size() != 1)
   {
      publishError("setClickNotifications", "number of parameters != 1");
   }
   else
   {
      try
      {
         switch (stoi(commandParameters[0]))
         {
            case 0:
               gattClicks.disableClicksNotifications();
               break;
            case 1:
               gattClicks.enableClicksNotifications();
               break;
            default:
               publishError("setClickNotifications", "parameter wrong value");
         }
      }
      catch(std::invalid_argument &e)
      {
          publishError("setClickNotifications", "invalid format argument");
      }
   }
}

void BLEclicksMQTT::temperature(const parameters_t& commandParameters)
{
  if (commandParameters.size() != 1)
  {
     publishError("temperature", "number of parameters != 1");
  }
  else
  {
     try
     { 
        if(stoi(commandParameters[0]) < 100 && stoi(commandParameters[0]) > 0){
           temp = stoi(commandParameters[0]);
           gattClicks.temperature(stoi(commandParameters[0]));
           shClicks.temperature(temp, humi, pres);
        }
        else{
           gattClicks.error();
           shClicks.error();
           publishError("temperature", "parameter wrong value");
        }
     }
     catch(std::invalid_argument &e)
     {
         publishError("temperature", "invalid format argument");
     }
  }
}

void BLEclicksMQTT::humidity(const parameters_t& commandParameters)
{
   if (commandParameters.size() != 1)
  {
     publishError("humidity", "number of parameters != 1");
  }
  else
  {
     try
     {
        if(stoi(commandParameters[0]) < 100 && stoi(commandParameters[0]) > 0){
           humi = stoi(commandParameters[0]);
           gattClicks.humidity(stoi(commandParameters[0]));
           shClicks.temperature(temp, humi, pres);
        }
        else{
           gattClicks.error();
           shClicks.error();
           publishError("humidity", "parameter wrong value");
        }
     }
     catch(std::invalid_argument &e)
     {
         publishError("humidity", "invalid format argument");
     }
  }
}

void BLEclicksMQTT::pressure(const parameters_t& commandParameters)
{
  if (commandParameters.size() != 1)
  {
     publishError("pressure", "number of parameters != 1");
  }
  else
  {
     try
     {
        if(stoi(commandParameters[0]) < 100 && stoi(commandParameters[0]) > 0){
           pres = stoi(commandParameters[0]);
           gattClicks.pressure(stoi(commandParameters[0]));
           shClicks.temperature(temp, humi, pres);
        }
        else{
           gattClicks.error();
           shClicks.error();
           publishError("pressure", "parameter wrong value");
        }
     }
     catch(std::invalid_argument &e)
     {
         publishError("temperature", "invalid format argument");
     }
  }
}

void BLEclicksMQTT::state(const parameters_t& commandParameters)
{
   if (commandParameters.size() != 1)
   {
      publishError("start", "number of parameters != 1");
   }
   else
   {
      try
      {
         switch (stoi(commandParameters[0]))
         {
            case 0:
               jspressed();
               start_state = true;
               gattClicks.start();
               break;
            case 1:
            if(start_state){
               start_state = false;
               gattClicks.pause();
               shClicks.pause(temp, pres ,humi);
            }
               break;
            case 2:
               start_state = false;
               gattClicks.stop();
               shClicks.stop();
               break;
            default:
               publishError("start", "parameter wrong value");
         }
      }
      catch(std::invalid_argument &e)
      {
          publishError("start", "invalid format argument");
      }
   }
}

void BLEclicksMQTT::error(const parameters_t& commandParameters)
{
  if (commandParameters.size() != 1)
  {
     publishError("temperature", "number of parameters != 1");
  }
  else
  {
     try
     {
           gattClicks.error();
           shClicks.error();
     }
     catch(std::invalid_argument &e)
     {
         publishError("temperature", "invalid format argument");
     }
  }
}

void BLEclicksMQTT::jsup()
{
   if(line > 0){
      line--;
      shClicks.showLine(line);
   }

}

void BLEclicksMQTT::jsdown()
{

   if(line < 3){
      line++;
      shClicks.showLine(line);
   }
}

void BLEclicksMQTT::jsright()
{
switch (line)
         {
            case 0:
           if(temp < 90){
            temp = temp + 10;
           gattClicks.temperature(temp);
           shClicks.temperature(temp, humi, pres);
           }
           else{
               publishError("Right joystick temp", "value out of scoop");              
           }
               break;

            case 1:            
           if(humi < 90){
            humi = humi + 10;
           gattClicks.humidity(humi);
           shClicks.temperature(temp, humi, pres);
           }
           else{
               publishError("Right joystick humi", "value out of scoop");              
           }
               break;

            case 2:
           if(pres < 90){
            pres = pres + 10;
           gattClicks.pressure(pres);
           shClicks.temperature(temp, humi, pres);
           }
           else{
               publishError("Right joystick pres", "value out of scoop");              
           }
               break;
            default:
               publishError("start", "parameter wrong value");
         }
}

void BLEclicksMQTT::jsleft()
{
switch (line)
         {
            case 0:
           if(temp > 10){
            temp = temp - 10;
           gattClicks.temperature(temp);
           shClicks.temperature(temp, humi, pres);
           }
           else{
               publishError("Right joystick temp", "value out of scoop");              
           }
               break;

            case 1:            
           if(humi > 10){
            humi = humi - 10;
           gattClicks.humidity(humi);
           shClicks.temperature(temp, humi, pres);
           }
           else{
               publishError("Right joystick humi", "value out of scoop");              
           }
               break;

            case 2:
           if(pres > 10){
            pres = pres - 10;
           gattClicks.pressure(pres);
           shClicks.temperature(temp, humi, pres);
           }
           else{
               publishError("Right joystick pres", "value out of scoop");              
           }
               break;
            default:
               publishError("start", "parameter wrong value");
         }
}

void BLEclicksMQTT::jspressed()
{

double pres1 = shClicks.get_pressure();
humi = shClicks.get_humidity();
double temp1 = shClicks.get_temperature();
temp = (int)temp1;
pres = (int)pres1  - 1000;

           
           shClicks.temperature(temp, humi, pres);
           gattClicks.temperature(temp);
           gattClicks.humidity(humi);
           gattClicks.pressure(pres);

   publishAddition("pixel", "back to 0 0");
}