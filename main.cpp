#include <stdio.h>
#include <string.h>
#include <csignal>
#include <unistd.h>

#include "gatt_wrapper.h"

#include "AppInfo.h"
#include "BLEclicksMQTT.h"
#include "BLEconfig.h"
#include "GATTwrapper.h"
#include "MQTTconfig.h"
#include "Pixel.h"
#include "SenseHAT.h"
#include <iostream>

using namespace std;

volatile sig_atomic_t receivedSIGINT{false};

void handleSIGINT(int /* s */)
{
   receivedSIGINT = true;
}

int main(int argc, char **argv)
{
   int majorMosquitto{0};
   int minorMosquitto{0};
   int revisionMosquitto{0};

   signal(SIGINT, handleSIGINT);

   try
   {
      string blePublicAddress = BLE_MAC;
      string mqttBroker{MQTT_LOCAL_BROKER};
      int mqttBrokerPort{MQTT_LOCAL_BROKER_PORT};

      switch (argc)
      {
         case 1:
            break;
         case 2:
            blePublicAddress = string(argv[1]);
            // Using MQTT_LOCAL_BROKER and MQTT_LOCAL_BROKER_PORT
            break;
         case 3:
            blePublicAddress = string(argv[1]);
            // Using MQTT_LOCAL_BROKER_PORT
            mqttBroker = string(argv[2]);
            break;
         case 4:
            blePublicAddress = argv[1];
            mqttBroker = string(argv[2]);
            mqttBrokerPort = stoi(argv[3]);
            break;
         default:
            cerr << "\nERROR command line arguments:\n"
                    "\t" APPNAME " <BLE public address>\n"
                    "\t" APPNAME " <BLE public address> <URL broker>\n"
                    "\t" APPNAME " <BLE public address> <URL broker> <broker port>"
                    "\n\n";
            exit(EXIT_FAILURE);
      }

      cout << "-- MQTT application: " << APPNAME_VERSION << "  ";
      mosqpp::lib_init();
      mosqpp::lib_version(&majorMosquitto, &minorMosquitto, &revisionMosquitto);
      cout << "uses Mosquitto lib version "
           << majorMosquitto
           << '.' << minorMosquitto
           << '.' << revisionMosquitto << endl;

      cout << "-- BLE " << blePublicAddress << "[clicks] start" << endl;

      BLEclicksMQTT bleclicksmqtt{blePublicAddress,"Composite_Control","RaspberryPi", mqttBroker, mqttBrokerPort};

      bleclicksmqtt.gattClicks.debugMSG(__func__, "MQTT and BLE are ready");
      bleclicksmqtt.shClicks.setLed_MQTTconnected(Pixel::RED);

      // 'clients' is an initializer_list
      auto clients = {static_cast<mosqpp::mosquittopp*>(&bleclicksmqtt)};

      while (!receivedSIGINT)
      {
         // Checking rc for reconnection
         for (auto client: clients)
         {
            int rc = client->loop();
            if (rc)
            {
               bleclicksmqtt.gattClicks.debugMSG(__func__, "ERROR: MQTT reconnect");
               bleclicksmqtt.shClicks.setLed_MQTTconnected(Pixel::RED);
               client->reconnect();
            }
            else
            {
               //bleclicksmqtt.gattClicks.debugMSG(__func__, "OK: MQTT reconnect");
               bleclicksmqtt.shClicks.setLed_MQTTconnected(Pixel::GREEN);
            }
         }
         
      }
      
   }
   catch(std::exception &e)
   {
      cerr << APPNAME " Exception: " << e.what() << endl;
   }
   catch(...)
   {
      cerr << APPNAME " UNKNOWN exception" << endl;
   }

   return 0;
}
