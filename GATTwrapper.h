#ifndef GATTWRAPPER_H
#define GATTWRAPPER_H

extern "C"
{
#include "gatt_wrapper.h"
}

#include <atomic>
#include <condition_variable>
#include <map>
#include <mutex>
#include <string>
#include <thread>

class CommandProcessor;

class GATTwrapper
{
public:
   GATTwrapper(CommandProcessor &cmdpr,
               int nTriesConnecting,
               const std::string &BLEaddress);
   GATTwrapper(const GATTwrapper &other) = delete;
   GATTwrapper& operator=(const GATTwrapper &other) = delete;
   virtual ~GATTwrapper();

   void debugMSG(const char src[], const char msg[]) const;
   void requestConnect() const;
   bool isConnected() const { return isConnected_; }
   void requestPrimaryServiceDiscovery() const;
   void showPrimaryServiceDiscovery() const;
   void requestManufacturingNameString() const;
   void requestSerialNumber() const;

protected:
   CommandProcessor &cmdp_;
   volatile std::atomic<bool> isConnected_;

   virtual void connectCB();
   virtual void charsCB(const char *uuid_128, unsigned short char_handle,
                        unsigned char properties, unsigned short value_handle);
   virtual void charDescCB(unsigned short attribute_handle,
                           const char *uuid_128);
   virtual void charReadHndCB(unsigned char *data, int len);
   virtual void charWriteReqCB(int succesful);
   virtual void charReadUuidCB(unsigned short value_handle,
                               unsigned char *data, int len);
   virtual void notifyCB(unsigned short value_handle,
                         unsigned char *data, int len);

private:
   const std::string BLEaddress_;
   const int nTriesConnecting_;
   volatile std::atomic<bool> tryConnecting_;
   volatile std::atomic<bool> isResponding_{true};
   mutable std::mutex mtxDBG_;
   mutable std::mutex mtxWRITE_;
   mutable std::mutex mtxREAD_;
   std::condition_variable cvAsyncResponses_;
   std::mutex mtxAsyncResponse_;
   std::thread handleBLEconnecting_;
   std::thread handleBLEasyncResponses_;
   std::map<unsigned short, std::string> primaryServices_;

   void BLEconnecting();
   void BLEasyncResponses();
   void BLEwrite(const char data[]) const;
   void BLEwrite(const std::string& data) const { BLEwrite(data.c_str()); };
   std::string BLEread() const;
   char asciitoint(const char c);
};

#endif
