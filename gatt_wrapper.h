// ----------------------------------------------------------------------------
// Wrapper for gatttool
//
// By Hugo Arends

// Jos Onokiewicz: removed threading and callback functions,
//                 now the are available in a C++ class wrapper.
//
// October 2017
// ----------------------------------------------------------------------------

#ifndef _GATT_WRAPPER_H_
#define _GATT_WRAPPER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>

// ----------------------------------------------------------------------------
// Definitions
// ----------------------------------------------------------------------------
// Macro for debugging
#define DBG(a,b) printf(" -- [%-20s] %s", a, b)
//#define DBG(a,b) //

void gatt_start(void);
void gatt_write(const char *s);
int  gatt_exit(void);
void gatt_read(char *r);

#ifdef __cplusplus
}
#endif

#endif // _GATT_WRAPPER_H_
