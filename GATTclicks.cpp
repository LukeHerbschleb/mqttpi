#include "GATTclicks.h"
#include "BLEconfig.h"
#include "CommandProcessor.h"
#include "Pixel.h"
#include "SHclicks.h"

#include <cstring>
#include <string>

GATTclicks::GATTclicks(SHclicks &shClicks,
                       CommandProcessor &cmdpr,
                       int nTriesConnecting,
                       const std::string &BLEaddress):
   GATTwrapper(cmdpr, nTriesConnecting, BLEaddress),
   shClicks{shClicks},
   ias_value_handle{0x0000},
   clicks_value_handle{0x0000},
   manufacturer_name_string_value_handle{0x0000},
   serial_number_string_value_handle{0x0000}
{}

void GATTclicks::connectCB()
{
   debugMSG(__func__, "is connected");
   isConnected_ = true;
   shClicks.setLed_BLEconnected(Pixel::GREEN);
}

void GATTclicks::charsCB(const char *uuid_128, unsigned short char_handle,
                         unsigned char properties, unsigned short value_handle)
{
   debugMSG(__func__, uuid_128);
   // Get handle for IAS characteristic?
   if(0 == strcmp(uuid_128, "00002a06-0000-1000-8000-00805f9b34fb"))
   {
      char s[80];
      sprintf(s,"ias characteristic handle: 0x%04x", char_handle);
      debugMSG(__func__, s);
      sprintf(s,"ias value handle: 0x%04x", value_handle);
      debugMSG(__func__, s);
      ias_value_handle = value_handle;
   }

   // Get handle for Manufacturer Name String characteristic?
   if(0 == strcmp(uuid_128, UUID128_C_MNS))
   {
      char s[80];
      sprintf(s, "manufacturer name string characteristic handle: 0x%04x",
              char_handle);
      debugMSG(__func__, s);
      sprintf(s, "manufacturer name string value handle: 0x%04x", value_handle);
      debugMSG(__func__, s);
      manufacturer_name_string_value_handle = value_handle;
   }

   // Get handle for Serial Number String characteristic?
   if(0 == strcmp(uuid_128, "00002a25-0000-1000-8000-00805f9b34fb"))
   {
      char s[80];
      sprintf(s,"serial number string characteristic handle: 0x%04x", char_handle);
      debugMSG(__func__, s);
      sprintf(s,"serial number string value handle: 0x%04x", value_handle);
      debugMSG(__func__, s);
      serial_number_string_value_handle = value_handle;
   }

   // Get handle for Clicks characteristic?
   if(0 == strcmp(uuid_128, UUID128_CC_CLICKS))
   {
      char s[80];
      sprintf(s,"clicks characteristic handle: 0x%04x", char_handle);
      debugMSG(__func__, s);
      sprintf(s,"clicks value handle: 0x%04x", value_handle);
      debugMSG(__func__, s);
      sprintf(s,"clicks CCCD handle: 0x%04x", value_handle+1);
      debugMSG(__func__, s);
      clicks_value_handle = value_handle;
   }
}

void GATTclicks::charDescCB(unsigned short attribute_handle,
                            const char *uuid_128)
{
   char s[80];
   sprintf(s,"attribute handle: 0x%04x, uuid: %s", attribute_handle, uuid_128);
   debugMSG(__func__, s);
}

void GATTclicks::charReadHndCB(unsigned char *data, int len)
{
    char s[40] = {'\0'};
    int i;

    // Copy the data
    for(i=0; i<len; i++)
        s[i] = static_cast<char>(data[i]);

    s[i++] = '\n';
    s[i] = '\0';
    debugMSG(__func__, s);
}

void GATTclicks::charWriteReqCB(int succesful)
{
    char s[20] = {'\0'};
    sprintf(s,"succesful = %d", succesful);
    debugMSG(__func__, s);
}

void GATTclicks::charReadUuidCB(unsigned short value_handle,
                                 unsigned char *data, int len)
{
    // Manufacturer Name String
    if(value_handle == manufacturer_name_string_value_handle)
    {
        char s[40] = {'\0'};
        int i;

        // Copy the data
        for(i=0; i<len; i++)
            s[i] = static_cast<char>(data[i]);

        strcpy(&s[i], " (Manufacturer Name String)");
        debugMSG(__func__, s);
    }

    // Serial Number String
    if(value_handle == serial_number_string_value_handle)
    {
       char s[40] = {'\0'};
       int i;

      // Copy the data
      for(i=0; i<len; i++)
         s[i] = static_cast<char>(data[i]);

      strcpy(&s[i], " (Serial Number String)");
      debugMSG(__func__, s);
    }
}

void GATTclicks::notifyCB(unsigned short value_handle,
                          unsigned char *data, int len)
{
   char s[20] = {'\0'};
   // Value handle for Clicks characteristic?
   if(value_handle == clicks_value_handle)
   {
      sprintf(s,"count: %d", clicks);
      debugMSG(__func__, s);
      // MQTT notify
      cmdp_.publishAddition("count", s);
      GATTclicks::error();

    }
}

void GATTclicks::enableClicksNotifications()
{
   char s[80];
   // If value handle is valid, enabele clicks notifications
   debugMSG(__func__, "");
   if(clicks_value_handle != 0x0000)
   {
      // Add one to the clicks value handle, because we need to set the CCCD
      sprintf(s,"char-write-req %04x 0100\n", clicks_value_handle+1);
      gatt_write(s);
      debugMSG(__func__, "");
   }
}

void GATTclicks::disableClicksNotifications()
{
   static const char src[] = "GATTwrapper::disableClicksNotifications";
   char s[80];
   // If value handle is valid, enabele clicks notifications
   debugMSG(src, "set clicks notifications disabling");
   if(clicks_value_handle != 0x0000)
   {
      // Add one to the clicks value handle, because we need to set the CCCD
      sprintf(s,"char-write-req %04x 0000\n", clicks_value_handle + 1);
      gatt_write(s);
      debugMSG(src, "set clicks notifications disabled");
   }
}

void GATTclicks::setAlertLevel(int level)
{
   static const char src[] = "GATTwrapper::setAlertLevel";
   char s[80];
   // If value handle is valid, enabele clicks notifications
   debugMSG(src, "update alert level");
   if(ias_value_handle != 0x0000)
   {
      // Use char-write-cmd
      sprintf(s,"char-write-cmd %04x %02d\n", ias_value_handle, level);
      gatt_write(s);
      debugMSG(src, "alert level updated");
   }
}

void GATTclicks::temperature(int temp)
{
   char s[80];
   // If value handle is valid, enabele clicks notifications
      sprintf(s,"%d", temp);
      debugMSG(__func__, s);
      // MQTT notify
      cmdp_.publishAddition("temperature", s);
}

void GATTclicks::humidity(int humi)
{
   char s[80];
   // If value handle is valid, enabele clicks notifications
      sprintf(s,"%d", humi);
      debugMSG(__func__, s);
      // MQTT notify
      cmdp_.publishAddition("humidity", s);
}

void GATTclicks::pressure(int pres)
{
   char s[80];
   // If value handle is valid, enabele clicks notifications
      sprintf(s,"%d", pres);
      debugMSG(__func__, s);
      // MQTT notify
      cmdp_.publishAddition("pressure", s);
}

void GATTclicks::start()
{
   char s[80];
   // If value handle is valid, enabele clicks notifications
      // MQTT notify
      cmdp_.publishAddition("ledgreen", "1");
}

void GATTclicks::pause()
{
   char s[80];
   // If value handle is valid, enabele clicks notifications
      // MQTT notify
      cmdp_.publishAddition("ledyellow", "1");
}

void GATTclicks::stop()
{
   char s[80];
   // If value handle is valid, enabele clicks notifications
      // MQTT notify
      cmdp_.publishAddition("ledred", "1");
}

void GATTclicks::error()
{
   char s[80];
   // If value handle is valid, enabele clicks notifications
      sprintf(s,"%d", clicks);
      // MQTT notify      
      cmdp_.publishAddition("temperature",   "0");
      cmdp_.publishAddition("humidity",      "0");
      cmdp_.publishAddition("pressure",      "0");
      
      shClicks.error();
}
