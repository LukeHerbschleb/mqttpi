#ifndef SHCLICKS_H
#define SHCLICKS_H

#include "SenseHAT.h"

class Pixel;

/// \todo Add documentation text.
class SHclicks: public SenseHAT
{
public:
   SHclicks();
   virtual ~SHclicks();
   // Display
   void initDisplay();
   void setLed_MQTTconnected(const Pixel &pixel);
   void setLed_BLEconnected(const Pixel &pixel);
   void setLed_BLElogo(const Pixel &pixel);
   void error();
   void temperature(int temp, int humi, int pres);
   void pause(int temp, int humi, int pres);
   void stop();
   void showLine(int line);
};

#endif
