#include "BLEconfig.h"
#include "CommandProcessor.h"
#include "GATTwrapper.h"
#include <iomanip>
#include <iostream>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>

using namespace std;

GATTwrapper::GATTwrapper(CommandProcessor &cmdp,
                         int nTriesConnecting,
                         const std::string &BLEaddress):
   cmdp_{cmdp},
   isConnected_{false},
   BLEaddress_{BLEaddress},
   nTriesConnecting_{nTriesConnecting},
   tryConnecting_{true},
   mtxDBG_{},
   mtxWRITE_{},
   mtxREAD_{},
   cvAsyncResponses_{},
   mtxAsyncResponse_{},
   handleBLEconnecting_{&GATTwrapper::BLEconnecting, this},
   handleBLEasyncResponses_{&GATTwrapper::BLEasyncResponses, this},
   primaryServices_{}
{ }

GATTwrapper::~GATTwrapper()
{
   tryConnecting_ = false;
   sleep(1);
   if(handleBLEconnecting_.joinable())
   {
      handleBLEconnecting_.join();
   }
   // Stop gatttool
   BLEwrite("disconnect\n");
   debugMSG(__func__, "disconnect");
   sleep(2);
   BLEwrite("DTOR\n");
   debugMSG(__func__, "write DTOR");
   sleep(2);
   gatt_exit();
   sleep(3);
   if(not isResponding_ and handleBLEasyncResponses_.joinable())
   {
      handleBLEasyncResponses_.join();
   }
}

void GATTwrapper::debugMSG(const char src[], const char msg[]) const
{
   std::lock_guard<std::mutex> lg{mtxDBG_};
   cerr << "[" << setw(40) << left << src << "] " << msg << endl;
}

void GATTwrapper::requestConnect() const
{
   std::string connectCmd{"connect " + BLEaddress_ + "\n"};
   BLEwrite(connectCmd.c_str());
   debugMSG(__func__, connectCmd.c_str());
}

void GATTwrapper::requestPrimaryServiceDiscovery() const
{
   BLEwrite("primary\n");
   debugMSG(__func__, "");
}

void GATTwrapper::showPrimaryServiceDiscovery() const
{
   debugMSG(__func__, "");
   for(auto& service: primaryServices_)
   {
      cerr << "service handle: 0x" << setw(4) << setfill('0') << hex
           << service.first << " uuid: " << service.second << endl;
   }
   cerr << setfill(' ');
}

void GATTwrapper::requestManufacturingNameString() const
{
   BLEwrite("char-read-uuid 2a29\n");
   debugMSG(__func__, "");
}

void GATTwrapper::requestSerialNumber() const
{
   BLEwrite("char-read-uuid 2a25\n");
   debugMSG(__func__, "");
}

void GATTwrapper::BLEconnecting()
{
   debugMSG(__func__, "BLE connecting started");
   gatt_start();
   debugMSG(__func__, "BLE started");

   requestConnect();
   for(int i = 0; i < nTriesConnecting_
                  and !isConnected_
                  and tryConnecting_;
                  i++)
   {
      //isConnected_ = connected;
      if (not isConnected_)
      {
         debugMSG(__func__, "Trying to connect...");
         std::this_thread::sleep_for(2s);
      }
   }
   if(not isConnected_)
   {
      debugMSG(__func__,
               "BLE Unable to connect, "
               "reset the BLE peripheral and try again");
   }
   else
   {
      debugMSG(__func__, "BLE is connected");
      // Request all primary services
      BLEwrite("primary\n");
      std::this_thread::sleep_for(2s);
      showPrimaryServiceDiscovery();
      // Get all the characteristics and wait for a while to allow the
      // application to handle the result
      BLEwrite("characteristics\n");
      debugMSG(__func__, "request characteristics");
   }
   debugMSG(__func__, "exit thread function");
}

void GATTwrapper::BLEasyncResponses()
{
   debugMSG(__func__, "BLE async response started");

   char s[120];

   // Make sure the thread can be cancelled, because this happens in the
   // function gatt_exit().
   //pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);

   string response;
   while(isResponding_)
   {
      //std::unique_lock<std::mutex> ul(mtxAsyncResponse_);
      //cvAsyncResponses_.wait(ul, [this]{ return isConnected_; });
      response = BLEread();
      strcpy(s, response.c_str());

      // Callback function for the following command:
      // [ ... ][LE]> connect <mac>
      if(response.find("Connection successful") != std::string::npos)
      {
         debugMSG(__func__, ("RESPONSE = '" + response + "' callback").c_str());
         connectCB();
         //if(gatt_callbacks_.gatt_connect_cb)
         //   (*( gatt_callbacks_.gatt_connect_cb))(1);
      }

      // Callback function for the following command:
      // [ ... ][LE]> primary
      //if(strstr(s,", end grp handle: "))
      if(response.find(", end grp handle: ") != std::string::npos)
      {
         char *p;
         unsigned short service_handle;
         unsigned short end_grp_handle;
         char uuid[40];
         int len=0;

         // Get the service handle from the string
         p = strstr(s,"attr handle: 0x") + 15;
         service_handle  = asciitoint(*p++) << 12;
         service_handle |= asciitoint(*p++) <<  8;
         service_handle |= asciitoint(*p++) <<  4;
         service_handle |= asciitoint(*p);

         // Get the end group handle from the string
         p = strstr(s,", end grp handle: 0x") + 21;
         end_grp_handle  = asciitoint(*p++) << 12;
         end_grp_handle |= asciitoint(*p++) <<  8;
         end_grp_handle |= asciitoint(*p++) <<  4;
         end_grp_handle |= asciitoint(*p);

         // Get the uuid from the string
         p = strstr(s,"uuid: ") + 6;
         while(*p != '\n')
         {
            uuid[len++] = *p++;
         }
         uuid[len] = '\0';

         // (*( gatt_callbacks_.gatt_primary_cb))(service_handle, end_grp_handle, uuid);
         primaryServices_[service_handle] = uuid;
      }
      // Callback function for the following command:
      // [ ... ][LE]> characteristics
      //if(strstr(s,"char properties: "))
      if(response.find("char properties: ") != std::string::npos)
      {
         char *p;
         unsigned short char_handle;
         unsigned short value_handle;
         char uuid[40];
         unsigned char properties;
         int len=0;

         // Get the characteristics handle from the string
         p = strstr(s,"handle: 0x") + 10;
         char_handle  = asciitoint(*p++) << 12;
         char_handle |= asciitoint(*p++) <<  8;
         char_handle |= asciitoint(*p++) <<  4;
         char_handle |= asciitoint(*p);

         // Get the value handle from the string
         p = strstr(s,"value handle: 0x") + 16;
         value_handle  = asciitoint(*p++) << 12;
         value_handle |= asciitoint(*p++) <<  8;
         value_handle |= asciitoint(*p++) <<  4;
         value_handle |= asciitoint(*p);

         // Get the properties from the string
         p = strstr(s,"char properties: 0x") + 19;
         properties  = asciitoint(*p++) << 4;
         properties |= asciitoint(*p);

         // Get the uuid from the string
         p = strstr(s,"uuid: ") + 6;
         while(*p != '\n')
         {
            uuid[len++] = *p++;
         }
         uuid[len] = '\0';
         // Return the result
         charsCB(uuid, char_handle, properties, value_handle);
      }
      // Callback function for the following command:
      // [ ... ][LE]> char-desc
      //if(strstr(s,"handle: ") && strstr(s,", uuid: ") && strlen(s) == 62)
      if(response.find("handle: ") != std::string::npos
         and response.find(", uuid: ") != std::string::npos
         and response.size() == 62)
      {
         {
            char *p;
            unsigned short handle;
            char uuid[40];
            int len=0;

            // Get the handle from the string
            p = strstr(s,"handle: 0x") + 10;
            handle  = asciitoint(*p++) << 12;
            handle |= asciitoint(*p++) <<  8;
            handle |= asciitoint(*p++) <<  4;
            handle |= asciitoint(*p);

            // Get the uuid from the string
            p = strstr(s,"uuid: ") + 6;

            while(*p != '\n')
            {
               uuid[len++] = *p++;
            }
            uuid[len] = '\0';
            // Return the result
            charDescCB(handle, uuid);
         }
      }
      // Callback function for the following command:
      // [ ... ][LE]> char-read-hnd
      //if(strstr(s,"Characteristic value/descriptor: "))
      if(response.find("Characteristic value/descriptor: ") != std::string::npos)
      {
         {
            char *p;
            char c;
            unsigned char data[40];
            int len=0;

            // Get the value from the string
            p = strstr(s,"Characteristic value/descriptor: ") + 33;
            while(*p != '\n')
            {
               c  = asciitoint(*p) << 4;
               p++;
               c |= asciitoint(*p);
               p++;
               p++;
               data[len++] = c;
            }
            // Return the result
            charReadHndCB(data,len);
         }
      }
      // Callback function for the following command:
      // [ ... ][LE]> char-read-uuid
      //if(strstr(s,"handle: ") && strstr(s,"value: "))
      if(response.find("handle: ") != std::string::npos
         and response.find("value: ") != std::string::npos)
      {
         {
            char *p;
            char c;
            unsigned short handle;
            unsigned char data[40];
            int len=0;

            // Get the handle from the string
            p = strstr(s,"handle: 0x") + 10;
            handle  = asciitoint(*p++) << 12;
            handle |= asciitoint(*p++) <<  8;
            handle |= asciitoint(*p++) <<  4;
            handle |= asciitoint(*p);

            // Get the value from the string
            p = strstr(s,"value: ") + 7;
            while(*p != '\n')
            {
               c  = asciitoint(*p) << 4;
               p++;
               c |= asciitoint(*p);
               p++;
               p++;
               data[len++] = c;
            }
            // Return the result
            charReadUuidCB(handle,data,len);
         }
      }
      // Callback function for the following command:
      // [ ... ][LE]> char-write-req
      //if(strstr(s,"Characteristic value was written successfully"))
      if(response.find("Characteristic value was written successfully") != std::string::npos)
      {
         charWriteReqCB(1);
      }

      // Callback function for notifications
      if(response.find("Notification") != std::string::npos)
      {
         {
            char *p;
            char c;
            unsigned short handle;
            unsigned char data[40];
            int len=0;

            // Get the handle from the string
            p = strstr(s,"handle = 0x") + 11;
            handle  = asciitoint(*p++) << 12;
            handle |= asciitoint(*p++) <<  8;
            handle |= asciitoint(*p++) <<  4;
            handle |= asciitoint(*p);

            // Get the value from the string
            p = strstr(s,"value: ") + 7;
            while(*p != '\n')
            {
               c  = asciitoint(*p) << 4;
               p++;
               c |= asciitoint(*p);
               p++;
               p++;
               data[len++] = c;
            }
            // Return the result
            notifyCB(handle, data, len);
         }
      }

      if(response.find("DTOR") != std::string::npos)
      {
         isResponding_ = false;
      }
   }
   debugMSG(__func__, "exit thread function");
}

void GATTwrapper::BLEwrite(const char data[]) const
{
   std::lock_guard<std::mutex> lg{mtxWRITE_};
   gatt_write(data);
}

std::string GATTwrapper::BLEread() const
{
   char r[200] = {'\0'};
   std::lock_guard<std::mutex> lg{mtxREAD_};
   gatt_read(r);
   return r;
}

char GATTwrapper::asciitoint(const char c)
{
    char t = c - '0';

    if(t > 9)
        t -= 39;

    return t;
}

void GATTwrapper::connectCB()
{
   debugMSG(__func__, "not implemented");
}

void GATTwrapper::charsCB(const char *uuid_128, unsigned short char_handle,
                          unsigned char properties, unsigned short value_handle)
{
   debugMSG(__func__, "not implemented");
}

void GATTwrapper::charDescCB(unsigned short attribute_handle, const char *uuid_128)
{
   debugMSG(__func__, "not implemented");
}

void GATTwrapper::charReadHndCB(unsigned char *data, int len)
{
   debugMSG(__func__, "not implemented");
}

void GATTwrapper::charWriteReqCB(int succesful)
{
   debugMSG(__func__, "not implemented");
}

void GATTwrapper::charReadUuidCB(unsigned short value_handle,
                                 unsigned char *data, int len)
{
   debugMSG(__func__, "not implemented");
}

void GATTwrapper::notifyCB(unsigned short value_handle, unsigned char *data, int len)
{
   debugMSG(__func__, "not implemented");
}
